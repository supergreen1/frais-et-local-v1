<?php

namespace App\Controller;


use App\Form\CommentaireType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class CommentaireController extends AbstractController
{
    /**
     * @Route("/commentaire", name="commentaire")
     * @param Request $request
     * @return Response
     */
    public function index(EntityManagerInterface $em,Request $request)
    {

        
        
        
        
        $commentaireForm = $this->createForm(CommentaireType::class);
        
        $commentaireForm->handleRequest($request);


        if ($commentaireForm->isSubmitted() && $commentaireForm->isValid()) {
            $commentaire = $commentaireForm->getData();
//            dd($commentaire);


            $em->persist($commentaire);
            $em->flush();
            $this->addFlash('success', 'Votre commentaire a bien été envoyé');
            return $this->redirectToRoute('commentaire');
        }


        return $this->render('commentaire/index.html.twig', [
            'controller_name' => 'CommentaireController',
            'commentaireForm' => $commentaireForm->createView(),
        ]);
    }
}
