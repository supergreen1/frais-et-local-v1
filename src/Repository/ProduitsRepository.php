<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Produits;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Produits|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produits|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produits[]    findAll()
 * @method Produits[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Produits::class);
    }


    /**
     * @return Produits []
     */

    public function findListeCourse(): array
    {
        $query = $this
            ->createQueryBuilder('p')
            ->select('p', 'l')
            ->join('p.listeCourses', 'l')
            ->orderBy('l.id', 'DESC');

        return $query->getQuery()->getResult();
    }


    /**
     * Récurpère les produits en lien avec une recherche
     * @param SearchData $search
     * @return Produits[]
     */


    public function findSearch(SearchData $search): array

    {

        $query = $this
            ->createQueryBuilder('p')
            ->select('p', 't', 'c')
            ->where('p.available = 1')
            ->join('p.tags', 't')
            ->join('p.categories', 'c');

        if (!empty($search->q)) {
            $query = $query
                ->andWhere('p.title LIKE :q')
                ->setParameter('q', "%{$search->q}%");
        }

        if (!empty($search->categories)) {
            $query = $query
                ->andWhere('c.id IN (:categories)')
                ->setParameter('categories', $search->categories);
        }
        if (!empty($search->tags)) {
            $query = $query
                ->andWhere('t.id IN (:tags)')
                ->setParameter('tags', $search->tags);
        }
        return $query->getQuery()->getResult();
    }

    /**
     * @return Produits[]
     */

    public function findLatest(): array

    {
        return $this->findVisibleQuery()
            ->setMaxResults(8)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return QueryBuilder
     */

    private function findVisibleQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('p')
            ->Where('p.available = true');
    }


}
