<?php

namespace App\Controller;

use App\Entity\Client;
use App\Repository\ClientRepository;
use App\Repository\ProduitsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param SessionInterface $session
     * @param ProduitsRepository $repository
     * @return Response
     */
    public function index(SessionInterface $session, ProduitsRepository $repository, ClientRepository $clientRepository)
    {
        $listeCommentaire = $this->getDoctrine()->getRepository(Client::class)->findAll();
        $commentaire = [];
        $number = count($listeCommentaire);


        // On cherche si le producteur a déjà une image sinon on mets la photo à null
        for ($i = 0; $i <= $number - 1; $i++) {

            $commentaire[] =[
                'commentaire' =>  $listeCommentaire[$i]->getCommentaire(),
                'client' => $listeCommentaire[$i]->getFirstname()
            ];

        }



//        dd($listeCommentaire, $commentaire, $number, $this);


        $produits = $repository->findLatest();
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'produits' => $produits,
            'commentaires'=> $commentaire
        ]);
    }
}
