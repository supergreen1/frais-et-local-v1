<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerO6oBlFg\srcApp_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerO6oBlFg/srcApp_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerO6oBlFg.legacy');

    return;
}

if (!\class_exists(srcApp_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerO6oBlFg\srcApp_KernelDevDebugContainer::class, srcApp_KernelDevDebugContainer::class, false);
}

return new \ContainerO6oBlFg\srcApp_KernelDevDebugContainer([
    'container.build_hash' => 'O6oBlFg',
    'container.build_id' => '4d51d6bd',
    'container.build_time' => 1591030398,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerO6oBlFg');
