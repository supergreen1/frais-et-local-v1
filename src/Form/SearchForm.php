<?php


namespace App\Form;


use App\Data\SearchData;
use App\Entity\Categories;
use App\Entity\Tags;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchForm extends AbstractType
{

    /**
     * https://www.grafikart.fr/tutoriels/filtre-produit-symfony-1211
     * @param FormBuilderInterface $builder
     * @param array $options
     */


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('q', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                        'placeholder' => 'Recherche'
                ]
            ])

            ->add('categories', EntityType::class, [
                'label' => false,
                'required' => false,
                'class' => Categories::class,
                'expanded' => true,
                'multiple' => true
            ])

            ->add('tags', EntityType::class, [
                'label' => false,
                'required' => false,
                'class' => Tags::class,
                'expanded' => true,
                'multiple' => true
            ])
        ;
    }

    /**
     *
     * @param OptionsResolver $resolver
     */

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchData::class,
            'method' => 'GET',
            'csrf_protection' => false


        ]);
    }


    /**
     * @return string
     */

    public function getBlockPrefix()
    {
        return '';
    }


}
