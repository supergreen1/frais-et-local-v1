<?php


namespace App\Data;


use App\Entity\Categories;
use App\Entity\Tags;

class SearchData
{


    /**
     * https://www.grafikart.fr/tutoriels/filtre-produit-symfony-1211
     * @var string
     */

    public $q = '';


    /**
     * @var Categories[]
     */

    public $categories = [];


    /**
     * @var Tags []
     */

    public $tags = [];






}
