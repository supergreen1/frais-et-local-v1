<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/about' => [[['_route' => 'about', '_controller' => 'App\\Controller\\AboutController::index'], null, null, null, false, false, null]],
        '/cart' => [[['_route' => 'cart', '_controller' => 'App\\Controller\\CartController::index'], null, null, null, false, false, null]],
        '/checkout' => [[['_route' => 'checkout', '_controller' => 'App\\Controller\\CheckoutController::index'], null, null, null, false, false, null]],
        '/choice' => [[['_route' => 'choice', '_controller' => 'App\\Controller\\ChoiceController::index'], null, null, null, false, false, null]],
        '/user/client' => [[['_route' => 'user_client', '_controller' => 'App\\Controller\\ClientController::index'], null, null, null, false, false, null]],
        '/client/register' => [[['_route' => 'client', '_controller' => 'App\\Controller\\ClientController::register'], null, null, null, false, false, null]],
        '/commentaire' => [[['_route' => 'commentaire', '_controller' => 'App\\Controller\\CommentaireController::index'], null, null, null, false, false, null]],
        '/contact/map' => [[['_route' => 'contact_map', '_controller' => 'App\\Controller\\ContactController::map'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'home', '_controller' => 'App\\Controller\\HomeController::index'], null, null, null, false, false, null]],
        '/image/producteur' => [[['_route' => 'image_producteur', '_controller' => 'App\\Controller\\ImageProducteurController::index'], null, null, null, false, false, null]],
        '/image/producteur/#image' => [[['_route' => 'producteur_image', '_controller' => 'App\\Controller\\ImageProducteurController::index'], null, null, null, false, false, null]],
        '/newsletter/add' => [[['_route' => 'newsletter_add', '_controller' => 'App\\Controller\\NewsletterController::newsletter'], null, null, null, false, false, null]],
        '/user/producteur' => [[['_route' => 'user_producteur', '_controller' => 'App\\Controller\\ProducteurController::user'], null, null, null, false, false, null]],
        '/producteur/register' => [[['_route' => 'producteur_register', '_controller' => 'App\\Controller\\ProducteurController::Register'], null, null, null, false, false, null]],
        '/produit/add' => [[['_route' => 'produit_add', '_controller' => 'App\\Controller\\ProduitController::add'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/c(?'
                    .'|art/(?'
                        .'|edit([^/]++)(*:193)'
                        .'|add([^/]++)(*:212)'
                        .'|remove/([^/]++)(*:235)'
                    .')'
                    .'|lient/(\\d+)(*:255)'
                .')'
                .'|/activationClient/([^/]++)(*:290)'
                .'|/([^/]++)(*:307)'
                .'|/newsletter(*:326)'
                .'|/Producteur(*:345)'
                .'|/activationProducteur/([^/]++)(*:383)'
                .'|/produ(?'
                    .'|cteur/(?'
                        .'|(\\d+)(*:414)'
                        .'|([^/]++)(*:430)'
                    .')'
                    .'|it(?'
                        .'|(*:444)'
                        .'|/(?'
                            .'|(\\d+)(*:461)'
                            .'|(\\d+)/edit(*:479)'
                        .')'
                    .')'
                .')'
                .'|/([^/]++)(*:499)'
                .'|/log(?'
                    .'|in(?'
                        .'|(*:519)'
                        .'|prod(*:531)'
                    .')'
                    .'|out(*:543)'
                .')'
                .'|/oubli\\-(?'
                    .'|client(*:569)'
                    .'|Producteur(*:587)'
                .')'
                .'|/reset\\-pass(?'
                    .'|\\-client/([^/]++)(*:628)'
                    .'|/([^/]++)(*:645)'
                .')'
                .'|/shop(?'
                    .'|(*:662)'
                    .'|/(\\d+)(*:676)'
                .')'
                .'|/wishlist(?'
                    .'|(*:697)'
                    .'|/(?'
                        .'|add([^/]++)(*:720)'
                        .'|panier([^/]++)(*:742)'
                        .'|remove/([^/]++)(*:765)'
                    .')'
                .')'
                .'|/back(*:780)'
                .'|/media/cache/resolve/(?'
                    .'|([A-z0-9_-]*)/rc/([^/]++)/(.+)(*:842)'
                    .'|([A-z0-9_-]*)/(.+)(*:868)'
                .')'
                .'|/(fr|en|de|it|es|pt)/contact(*:905)'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        193 => [[['_route' => 'cart_edit', '_controller' => 'App\\Controller\\CartController::edit'], ['id'], null, null, false, true, null]],
        212 => [[['_route' => 'cart_add', '_controller' => 'App\\Controller\\CartController::add'], ['id'], null, null, false, true, null]],
        235 => [[['_route' => 'cart_remove', '_controller' => 'App\\Controller\\CartController::remove'], ['id'], null, null, false, true, null]],
        255 => [[['_route' => 'client_delete', '_controller' => 'App\\Controller\\ClientController::deleteClient'], ['id'], ['DELETE' => 0], null, false, true, null]],
        290 => [[['_route' => 'activationClient', '_controller' => 'App\\Controller\\ClientController::activation'], ['token'], null, null, false, true, null]],
        307 => [[['_route' => 'imageProducteur_delete', '_controller' => 'App\\Controller\\ImageProducteurController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        326 => [[['_route' => 'newsletter', '_controller' => 'App\\Controller\\NewsletterController::index'], [], null, null, false, false, null]],
        345 => [[['_route' => 'Producteur', '_controller' => 'App\\Controller\\ProducteurController::index'], [], null, null, false, false, null]],
        383 => [[['_route' => 'activationProducteur', '_controller' => 'App\\Controller\\ProducteurController::activation'], ['token'], null, null, false, true, null]],
        414 => [[['_route' => 'producteur_detail', '_controller' => 'App\\Controller\\ProducteurController::detailProducteur'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        430 => [[['_route' => 'producteur_delete', '_controller' => 'App\\Controller\\ProducteurController::deleteProducteur'], ['id'], ['DELETE' => 0], null, false, true, null]],
        444 => [[['_route' => 'produit', '_controller' => 'App\\Controller\\ProduitController::index'], [], null, null, false, false, null]],
        461 => [[['_route' => 'produit_detail', '_controller' => 'App\\Controller\\ProduitController::detail'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        479 => [[['_route' => 'produit_edit', '_controller' => 'App\\Controller\\ProduitController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        499 => [[['_route' => 'produit_delete', '_controller' => 'App\\Controller\\ProduitController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        519 => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityClientController::login'], [], null, null, false, false, null]],
        531 => [[['_route' => 'prod_login', '_controller' => 'App\\Controller\\SecurityProdController::login'], [], null, null, false, false, null]],
        543 => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityProdController::logout'], [], null, null, false, false, null]],
        569 => [[['_route' => 'forgotten_client', '_controller' => 'App\\Controller\\SecurityClientController::forgettenClient'], [], null, null, false, false, null]],
        587 => [[['_route' => 'forgotten_Producteur', '_controller' => 'App\\Controller\\SecurityProdController::forgottenPassProducteur'], [], null, null, false, false, null]],
        628 => [[['_route' => 'app_reset_password_client', '_controller' => 'App\\Controller\\SecurityClientController::resetPassword'], ['token'], null, null, false, true, null]],
        645 => [[['_route' => 'app_reset_password', '_controller' => 'App\\Controller\\SecurityProdController::resetPassword'], ['token'], null, null, false, true, null]],
        662 => [[['_route' => 'shop', '_controller' => 'App\\Controller\\ShopController::index'], [], null, null, false, false, null]],
        676 => [[['_route' => 'shop_detail', '_controller' => 'App\\Controller\\ShopController::detail'], ['id'], ['GET' => 0], null, false, true, null]],
        697 => [[['_route' => 'wishlist', '_controller' => 'App\\Controller\\WishlistController::index'], [], null, null, false, false, null]],
        720 => [[['_route' => 'wishlist_add', '_controller' => 'App\\Controller\\WishlistController::add'], ['id'], null, null, false, true, null]],
        742 => [[['_route' => 'panier', '_controller' => 'App\\Controller\\WishlistController::add_cart'], ['id'], null, null, false, true, null]],
        765 => [[['_route' => 'wishlist_remove', '_controller' => 'App\\Controller\\WishlistController::remove'], ['id'], null, null, false, true, null]],
        780 => [[['_route' => 'easyadmin', '_controller' => 'EasyCorp\\Bundle\\EasyAdminBundle\\Controller\\EasyAdminController::indexAction'], [], null, null, true, false, null]],
        842 => [[['_route' => 'liip_imagine_filter_runtime', '_controller' => 'Liip\\ImagineBundle\\Controller\\ImagineController::filterRuntimeAction'], ['filter', 'hash', 'path'], ['GET' => 0], null, false, true, null]],
        868 => [[['_route' => 'liip_imagine_filter', '_controller' => 'Liip\\ImagineBundle\\Controller\\ImagineController::filterAction'], ['filter', 'path'], ['GET' => 0], null, false, true, null]],
        905 => [
            [['_route' => 'contact', '_controller' => 'App\\Controller\\ContactController::index'], ['_locale'], null, null, false, false, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
