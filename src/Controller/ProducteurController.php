<?php

namespace App\Controller;

use App\Entity\ImageProducteur;
use App\Entity\Ligne;
use App\Entity\Producteur;
use App\Form\ImageProducteurType;
use App\Form\ProducteurType;
use App\Repository\ImageProducteurRepository;
use App\Repository\ProducteurRepository;
use App\Repository\ProduitsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ProducteurController extends AbstractController
{
    /**
     * @var ProducteurRepository
     */
    private $repository;

    /**
     * ProducteurController constructor.
     * @param ProducteurRepository $repository
     */

    public function __construct(ProducteurRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/Producteur", name="Producteur")
     * @param ImageProducteurRepository $imageProducteurRepository
     * @return Response
     */
    public function index(ImageProducteurRepository $imageProducteurRepository)
    {


        $imageProducteur = $imageProducteurRepository->findAll();
        $imageProd = new ImageProducteur();
        $producteur = $this->repository->findAll();
        $test = false;

        return $this->render('Producteur/index.html.twig', [
            'controller_name' => 'ProducteurController',
            'producteurs' => $producteur,
            'images' => $imageProducteur,
            'test' => $test

        ]);
    }

    /**
     * @Route("/user/producteur", name="user_producteur")
     * @param ProduitsRepository $produitsRepository
     * @param UserInterface $user
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function user(ProduitsRepository $produitsRepository, UserInterface $user, Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {

        $producteurRepo = $em->getRepository(Producteur::class);
        $id = $user->getId();
        $producteur = $producteurRepo->find($id);
        $produits = $produitsRepository->findAll();

        $producteurForm = $this->createForm(ProducteurType::class, $producteur);
        $producteurForm->handleRequest($request);

        $number = count($produits);
        $ajout = [];
        //On lie les produits du producteur au producteur
        for ($i = 0; $i <= $number - 1; $i++) {
            if ($producteur->getId() == $produits[$i]->getSeller()->getId()) {
                $ajout[$i] = $produits[$i];
            }
        }
        //Modification des données personnelles
        if ($producteurForm->isSubmitted() && $producteurForm->isValid()) {
            $password = $encoder->encodePassword($producteur, $producteur->getPassword());
            $producteur->setPassword($password);
            $em->persist($producteur);
            $em->flush();

        };
        return $this->render('user/producteur.html.twig', [
            'controller_name' => 'UserController',
            'produits' => $ajout,
            'producteur' => $user,
            'producteurForm' => $producteurForm->createView(),
        ]);
    }


    /**
     * @Route("/producteur/register", name="producteur_register")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @param \Swift_Mailer $mailer
     * @return RedirectResponse|Response
     */
    public function Register(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {
        $producteur = new Producteur();
        $producteurForm = $this->createForm(ProducteurType::class, $producteur);

        $producteurForm->handleRequest($request);

        if ($producteurForm->isSubmitted() && $producteurForm->isValid()) {
            $password = $encoder->encodePassword($producteur, $producteur->getPassword());
            $producteur->setPassword($password);
            $producteur->setAdmin(false);

            //on génère le token d'activation
            $producteur->setActivationToken(md5(uniqid()));

            $em->persist($producteur);
            $em->flush();

            $message = (new\Swift_Message('Activation du compte'))

                //On attribue l'expéditeur
                ->setFrom('votre@adresse.fr')

                //On attribue le destinataire
                ->setTo($producteur->getEmail())

                //On crée le message avec la vue Twig
                ->setBody(
                    $this->renderView(
                        'emails/activationProducteur.html.twig', ['token' =>$producteur->getActivationToken()]
                    ),
                    'text/html'
                );

            //On envoie le message
            $mailer->send($message);
            $this->addFlash('message', 'Le message a bien été envoyé');


            return $this->redirectToRoute('home');

        }

        return $this->render('producteur/register.html.twig', [
            'producteurForm' => $producteurForm->createView()
        ]);
    }


    /**
     * @route("/activationProducteur/{token}", name="activationProducteur")
     */

    public function activation($token, ProducteurRepository $repository)
    {

        //on vérifie si un utilisateur a ce token
        $producteur = $repository->findOneBy(['activation_token' => $token]);

//si aucun utilisateur n'existe avec ce token
        if (!$producteur) {
            //erreur 4041
            throw  $this->createNotFoundException(('Activation inconnue'));
        }
        //on supprime le token

        $producteur->setActivationToken(null);
        $em = $this->getDoctrine()->getManager();
        $em->persist($producteur);

        $em->flush();
//on envoie un message flash

        $this->addFlash('success', 'Votre compte est activé, vous pouvez maintenant vous connecter.');

        //on retourne à l'accueil
        return $this->redirectToRoute('prod_login');

    }


    /**
     * @param EntityManagerInterface $em
     * @param $id
     * @return Response
     * @Route("/producteur/{id}", name="producteur_detail", requirements={"id"="\d+"},
     *    methods={"GET","POST"} )
     */
    public function detailProducteur(EntityManagerInterface $em, $id, ProduitsRepository $produitsRepository)
    {

        //récupère l'ensemble des produit en BDD
        $produits = $produitsRepository->findAll();
        //récupère le producteur en BDD
        $producteurRepo = $em->getRepository(Producteur::class);
        $producteur = $producteurRepo->find($id);

        $number = count($produits);
        $ajout = [];

        //On lie les produits du producteur au producteur
        for ($i = 0; $i <= $number - 1; $i++) {
            if ($producteur->getId() == $produits[$i]->getSeller()->getId()) {
                $ajout[$i] = $produits[$i];
            }
        }

        return $this->render('producteur/detail.html.twig', [
            'producteur' => $producteur,
            'produits' => $ajout

        ]);
    }


    /**
     * @param Request $request
     * @param Producteur $producteur
     * @return RedirectResponse
     * @Route("/producteur/{id}", name="producteur_delete", methods={"DELETE"})
     */
    public function deleteProducteur(Request $request, Producteur $producteur): response
    {

        if ($this->isCsrfTokenValid('delete' . $producteur->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $session = new Session();
            $session->invalidate();
            $entityManager->remove($producteur);
            $entityManager->flush();

        }
        $this->addFlash('success', 'Votre compte" a été supprimé');
        return $this->redirectToRoute('home');

    }

    /**
     * @return Response
     * @Route("/producteur/recap", name="producteur_recap")
     */
    public function recap(){
        $lignes = $this->getDoctrine()->getRepository(Ligne::class)->findLineByProducer($this->getUser());


        return $this->render('producteur/recap.html.twig',[
            'lignes'=>$lignes
        ]);
    }

}
