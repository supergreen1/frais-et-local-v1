<?php

namespace App\Form;

use App\Entity\Producteur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProducteurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder



            ->add('civility', ChoiceType::class, [
                'label'=>"Civilité",
                'choices' => [
                    '' => 'vide',
                    'Madame' => 'Madame',
                    'Mademoiselle' => 'Mademoiselle',
                    'Monsieur' => 'Monsieur'
                ]

            ])
            ->add('username', TextType::class, [
                'required' => true,
                'label' => 'Choisir un identifiant'
            ])
            ->add('password', RepeatedType::class,[
                'type'=>PasswordType::class,
                'invalid_message'=>"Vous n'avez pas confirmé le même password",
                'first_options'=>[
                    'label'=>'Mot de passe'
                ],
                'second_options'=>[
                    'label'=>"confirmation du mot de passe"
                ]
            ])
            ->add('email',EmailType::class)
            ->add('siret', TextType::class)
            ->add('kBis', TextType::class)
            ->add('company', TextType::class, [
                'required' => true,
                'label' => 'Entreprise'
            ])
            ->add('firstname', TextType::class, [
                'required' => true,
                'label' => 'Prénom'
            ])
            ->add('lastname', TextType::class, [
                'required' => true,
                'label' => 'Nom'
            ])
            ->add('phone', TextType::class, [
                'required' => true,
                'label' => 'Téléphone'
            ])
            ->add('adress', TextType::class, [
                'required' => true,
                'label' => 'adresse'
            ])
            ->add('postalCode', TextType::class, [
                'required' => true,
                'label' => 'Code postal'
            ])
            ->add('city', TextType::class, [
                'required' => true,
                'label' => 'Ville',
                'attr'=>['autocomplete' => 'off']

            ])
            ->add('latitude', HiddenType::class)
            ->add('longitude', HiddenType::class)

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Producteur::class,
        ]);
    }
}
