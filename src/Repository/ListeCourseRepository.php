<?php

namespace App\Repository;

use App\Entity\ListeCourse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ListeCourse|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListeCourse|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListeCourse[]    findAll()
 * @method ListeCourse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListeCourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ListeCourse::class);
    }



    public function findListe(): array

    {
        return $this->findListeQuery()
            ->getQuery()
            ->getResult();
    }

    /**
     * @return QueryBuilder
     */

    private function findListeQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('l');

    }



}
