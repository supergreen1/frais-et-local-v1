<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200603092540 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE notation_produits');
        $this->addSql('ALTER TABLE client CHANGE activation_token activation_token VARCHAR(60) DEFAULT NULL, CHANGE reset_token reset_token VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE notation ADD produit_id INT DEFAULT NULL, CHANGE note note INT NOT NULL');
        $this->addSql('ALTER TABLE notation ADD CONSTRAINT FK_268BC95F347EFB FOREIGN KEY (produit_id) REFERENCES produits (id)');
        $this->addSql('CREATE INDEX IDX_268BC95F347EFB ON notation (produit_id)');
        $this->addSql('ALTER TABLE producteur CHANGE activation_token activation_token VARCHAR(60) DEFAULT NULL, CHANGE reset_token reset_token VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE produits CHANGE featured_image featured_image VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE notation_produits (notation_id INT NOT NULL, produits_id INT NOT NULL, INDEX IDX_6427FDFCCD11A2CF (produits_id), INDEX IDX_6427FDFC9680B7F7 (notation_id), PRIMARY KEY(notation_id, produits_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE notation_produits ADD CONSTRAINT FK_6427FDFC9680B7F7 FOREIGN KEY (notation_id) REFERENCES notation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notation_produits ADD CONSTRAINT FK_6427FDFCCD11A2CF FOREIGN KEY (produits_id) REFERENCES produits (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE client CHANGE activation_token activation_token VARCHAR(60) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE reset_token reset_token VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE notation DROP FOREIGN KEY FK_268BC95F347EFB');
        $this->addSql('DROP INDEX IDX_268BC95F347EFB ON notation');
        $this->addSql('ALTER TABLE notation DROP produit_id, CHANGE note note VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE producteur CHANGE activation_token activation_token VARCHAR(60) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE reset_token reset_token VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE produits CHANGE featured_image featured_image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
