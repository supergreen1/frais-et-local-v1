<?php

namespace App\Repository;

use App\Entity\Client;
use App\Entity\Commande;
use App\Entity\Producteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Commande|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commande|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commande[]    findAll()
 * @method Commande[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commande::class);
    }

    public function findMycommandes(Client $client){

        $em = $this->getEntityManager();

        // on crée la requête DQL
        $dql = "SELECT c
                FROM App\Entity\Commande c
                WHERE c.client = :client
                ORDER BY c.dateCommande ASC
            ";
        // on crée un objet Query
        $query = $em->createQuery($dql);
        $query->setParameter('client',$client);
        $query->setMaxResults(5);
        $query->setFirstResult(0);
        // on retourne le résultat
        return $query->getResult();
    }


}
