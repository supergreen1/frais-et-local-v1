<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientType;
use App\Repository\ClientRepository;
use App\Repository\ProduitsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ClientController extends AbstractController
{

    /**
     * @Route("/user/client", name="user_client")
     * @param UserInterface $user
     * @return Response
     */
    public function index(UserInterface $user, Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {

        $clientRepo = $em->getRepository(Client::class);

        $client = $clientRepo->find($user->getId());
        $clientForm = $this->createForm(ClientType::class, $client);

        $clientForm->handleRequest($request);

        if ($clientForm->isSubmitted() && $clientForm->isValid()) {

            $password = $encoder->encodePassword($client, $client->getPassword());
            $client->setPassword($password);
            $em->persist($client);
            $em->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('user/client.html.twig', [
            'controller_name' => 'UserController',
            'client' => $user,
            'clientForm' => $clientForm->createView()

        ]);
    }

    /**
     * @Route("/client/register", name="client")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @param \Swift_Mailer $mailer
     * @return Response
     */
    public function register(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {


        $client = new Client();
        $clientForm = $this->createForm(ClientType::class, $client);

        $clientForm->handleRequest($request);

        if ($clientForm->isSubmitted() && $clientForm->isValid()) {

            $client->setAdmin(false);
            $password = $encoder->encodePassword($client, $client->getPassword());
            $client->setPassword($password);

            //on génère le token d'activation
            $client->setActivationToken(md5(uniqid()));

            $em->persist($client);
            $em->flush();


            $message = (new\Swift_Message('Activation du compte'))

                //On attribue l'expéditeur
                ->setFrom('votre@adresse.fr')

                //On attribue le destinataire
                ->setTo($client->getEmail())

                //On crée le message avec la vue Twig
                ->setBody(
                    $this->renderView(
                        'emails/activationClient.html.twig', ['token' =>$client->getActivationToken()]
                    ),
                    'text/html'
                );

            //On envoie le message
            $mailer->send($message);
            $this->addFlash('message', 'Le message a bien été envoyé');

            return $this->redirectToRoute('home');

        }
        return $this->render('client/register.html.twig', [
            'clientForm' => $clientForm->createView()

        ]);
    }

    /**
     * @route("/activationClient/{token}", name="activationClient")
     * @param $token
     * @param ClientRepository $repository
     * @return RedirectResponse
     */


    public function activation($token, ClientRepository $repository)
    {
        //on vérifie si un utilisateur a ce token
        $client = $repository->findOneBy(['activation_token' => $token]);

//si aucun utilisateur n'existe avec ce token

        if (! $client) {
            //erreur 4041
            throw  $this->createNotFoundException(('Activation inconnue'));
        }
        //on supprime le token

        $client->setActivationToken(null);
        $em = $this->getDoctrine()->getManager();
        $em->persist( $client);
        $em->flush();
//on envoie un message flash

        $this->addFlash('success', 'Votre compte est activé, vous pouvez maintenant vous connecter.');

        //on retourne à l'accueil
        return $this->redirectToRoute('app_login');
    }


    /**
     * @param $id
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse
     * @Route("/client/{id}", name="client_delete", requirements={"id"="\d+"},
     *     methods={"DELETE"})
     */
    public function deleteClient( Request $request, Client $client )
    {

        if($this->isCsrfTokenValid('delete'.$client->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $session = new Session();
            $session->invalidate();
            $entityManager->remove($client);
            $entityManager->flush();

        }

        $this->addFlash('success', 'Votre compte" a été supprimé');
        return $this->redirectToRoute('home');


    }
}
