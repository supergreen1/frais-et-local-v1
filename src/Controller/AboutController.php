<?php

namespace App\Controller;

use App\Entity\Newsletter;
use App\Form\NewsletterType;
use App\Repository\ProduitsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class  AboutController extends AbstractController
{
    /**
     * @Route("/about", name="about")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $newsletter = new Newsletter();
        $newsletterForm = $this->createForm(NewsletterType::class, $newsletter);
        $newsletterForm->handleRequest($request);



        return $this->render('about/index.html.twig', [
            'controller_name' => 'AboutController',
            'newsletterForm'=> $newsletterForm->createView()

        ]);
    }
}
