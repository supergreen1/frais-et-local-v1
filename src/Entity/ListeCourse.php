<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\MakerBundle\Str;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ListeCourseRepository")
 */
class ListeCourse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produits", inversedBy="listeCourses")
     * @ORM\JoinColumn(nullable=false,
     *  onDelete="CASCADE"
     * )
     */
    private $Produit;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="listeCourses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $client;




    public function __toString()
    {
             return $this->quantity;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getProduit(): ?Produits
    {
        return $this->Produit;
    }

    public function setProduit(?Produits $Produit): self
    {
        $this->Produit = $Produit;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }



}
