<?php

namespace App\Controller;

use App\Entity\Producteur;
use App\Form\ResetPassType;
use App\Repository\ProducteurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityProdController extends AbstractController
{
    /**
     * @Route("/loginprod", name="prod_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/loginprod.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @route("/oubli-Producteur", name="forgotten_Producteur")
     * @param Request $request
     * @param ProducteurRepository $repository
     * @param \Swift_Mailer $mailer
     * @param TokenGeneratorInterface $tokenGenerator
     * @return RedirectResponse|Response
     */

    public function forgottenPassProducteur(Request $request, ProducteurRepository $repository, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator)
    {

        //On créer le formulaire
        $form = $this->createForm(ResetPassType::class);

        // on traite le formulaire
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //on récupère les données
            $donnees = $form->getData();

            //on cheerche si cet utilisateur a cet email

            $producteurrepo = $repository->findAll();
            $producteur = new Producteur();

            $number = count($producteurrepo);

            for ($i = 0; $i <= $number - 1; $i++) {

                if ($donnees['email'] == $producteurrepo[$i]->getEmail()) {
                    // On génère un token
                    $producteur = $producteurrepo[$i];

                    $token = $tokenGenerator->generateToken();

                    try {
                        $producteur->setResetToken($token);
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($producteur);
                        $entityManager->flush();
                    } catch (\Exception $e) {
                        $this->addFlash('danger', 'Une erreur est survenue : ' . $e->getMessage());
                        return $this->redirectToRoute('prod_login');
                    }
                }

            }

            // Si l'utilisateur n'existe pas
            if ($producteur->getEmail() == null) {

                // On envoie un message flash
                $this->addFlash('danger', 'Cette adresse n\'existe pas');

                return $this->redirectToRoute('prod_login');
            }
            //on génère l'URL de réinitialisation de mot de passe
            $url = $this->generateUrl('app_reset_password', ['token' => $token],
                UrlGeneratorInterface::ABSOLUTE_URL);
            // On envoie le message
            $message = (new \Swift_Message('Mot de passe oublié'))
                ->setFrom('votre@adresse.fr')
                ->setTo($producteur->getEmail())
                ->setBody(
                    "<p>Bonjour,</p><p>Pour activer votre mot de passe, veuillez cliquer sur le lien suivant : " . $url . '</p>',
                    'text/html'
                );
            $mailer->send($message);

            // On crée le message flash
            $this->addFlash('success', 'Un e-mail de réinitialisation a été envoyé sur votre boite mail');
            return $this->redirectToRoute('prod_login');
        }
        // On envoie vers la page de demande de l'e-mail
        return $this->render('security/forgotten_password.html.twig', ['emailForm' => $form->createView()]);
    }

    /**
     * @Route("/reset-pass/{token}", name="app_reset_password")
     * @param $token
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return RedirectResponse|Response
     */

    public function resetPassword($token, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {

        //on va chercher l'utilisateur avec le token fourni

        $producteur = $this->getDoctrine()->getRepository(Producteur::class)->findOneBy(['reset_token' => $token]);

        if (!$producteur) {
            $this->addFlash('danger', 'Token inconnue');
            return $this->redirectToRoute('prod_login');
        }
        if ($request->isMethod('POST')) {
            $producteur->setPassword($passwordEncoder->encodePassword($producteur, $request->request->get('password')));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($producteur);
            $entityManager->flush();
            $this->addFlash('success', 'Mot de passe modifié avec succès');

            return $this->redirectToRoute('prod_login');
        } else {
            return $this->render('security/reset_password.html.twig', ['token' => $token]);
        }


    }


}
