<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
class Contact{

    /**
     * @var string |null
     * @Assert\NotBlank()
     * Asset\Length(min=2, max=100)
     */
    private $firstname;


    /**
     * @var string |null
     * @Assert\NotBlank()
     * Asset\Length(min=2, max=100)
     */
    private $lastname;


      /**
     * @var string |null
       * @Assert\NotBlank()
     * Asset\Email()
     *
     */
    private $email;

    /**
     * @var string |null
     * @Assert\NotBlank()
     * Asset\Length(min=2, max=60)
     */
    private $subject;

    /**
     * @var string |null
     * @Assert\NotBlank()
     * Asset\Length(min=2)
     */
    private $message;

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string|null $firstname
     * @return Contact
     */
    public function setFirstname(?string $firstname): Contact
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string|null $lastname
     * @return Contact
     */
    public function setLastname(?string $lastname): Contact
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return Contact
     */
    public function setEmail(?string $email): Contact
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param string|null $subject
     * @return Contact
     */
    public function setSubject(?string $subject): Contact
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     * @return Contact
     */
    public function setMessage(?string $message): Contact
    {
        $this->message = $message;
        return $this;
    }



}
