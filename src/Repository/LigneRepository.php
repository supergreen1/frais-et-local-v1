<?php

namespace App\Repository;

use App\Entity\Ligne;
use App\Entity\Producteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ligne|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ligne|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ligne[]    findAll()
 * @method Ligne[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LigneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ligne::class);
    }

    public function findLineByProducer(Producteur $producteur){

        $em = $this->getEntityManager();

        //on crée la requête DQL

        $dql = "SELECT l 
               FROM App\Entity\Ligne l
               JOIN l.produit p
               WHERE p.seller = :producteur
               ORDER BY l.commande DESC 
               ";

        //on crée un objet Query

        $query = $em->createQuery($dql)
            ->setParameter('producteur',$producteur);
        return $query->getResult();

    }
}
