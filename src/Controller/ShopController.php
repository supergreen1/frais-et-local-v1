<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Produits;
use App\Form\ProduitsType;
use App\Form\SearchForm;
use App\Repository\ProduitsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class ShopController extends AbstractController
{

    /**
     * @var ProduitsRepository
     */
    private $repository;

    /**
     * ShopController constructor.
     * @param ProduitsRepository $repository
     */

    public function __construct(ProduitsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/shop", name="shop")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return RedirectResponse|Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $data = new SearchData();
        $form = $this->createForm(SearchForm::class, $data);
        $form->handleRequest($request);
        $produits = $paginator->paginate(
            $this->repository->findSearch($data),
            $request->query->getInt('page', 1),
            12
        );

        return $this->render('shop/index.html.twig', [
            'produits' => $produits,
            'form' => $form->createView()
        ]);
    }

    /**
     * /**
     * @Route("/shop/{id}", name="shop_detail",
     *     requirements={"id": "\d+"},
     *     methods={"GET"})
     * @param Produits $produit
     * @param ProduitsRepository $repository
     * @return RedirectResponse|Response
     */

    public function detail( Produits $produit , ProduitsRepository $repository)   {
        $produits = $repository->findLatest();
        return $this->render('shop/detail.html.twig', [
            'produit' => $produit,
            'produits' => $produits,
            'controller_name' => 'ShopController'
        ]);
    }


}
