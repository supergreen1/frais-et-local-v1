<?php

namespace App\Controller;

use App\Entity\Newsletter;
use App\Form\NewsletterType;
use Doctrine\ORM\EntityManagerInterface;
use http\Client\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class NewsletterController extends AbstractController
{
    /**
     * @Route("/newsletter", name="newsletter")
     */
    public function index()
    {
        $newsletter = new Newsletter();
        $newsletterForm = $this->createForm(NewsletterType::class, $newsletter);

        return $this->render('newsletter/index.html.twig', [
            'controller_name' => 'NewsletterController',
            'newsletterForm'=> $newsletterForm->createView()
        ]);
    }

    /**
     * @Route("/newsletter/add", name="newsletter_add")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse
     */


    public function newsletter(Request $request, EntityManagerInterface $em)
    {

        /*   Récupération de la liste des personnes inscrites en BDD et des données du formulaire  */
        $newsletter = new Newsletter();
        $newsletterForm = $this->createForm(NewsletterType::class, $newsletter);
        $newsletterForm->handleRequest($request);
        $liste = $this->getDoctrine()->getRepository(Newsletter::class)->findAll();

        /*   Vérification si la personne n'est pas déjà inscrite en BDD   */
        $number = count($liste);

        for ($i = 0; $i <= $number - 1; $i++) {

            if ($newsletter->getMail() == $liste[$i]->getMail()) {
                $this->addFlash('danger', 'Vous êtes déjà inscrit à la newsletter!');
                return $this->redirectToRoute('newsletter');
            }
        }

        /*   Insertion en BDD   */
        $em->persist($newsletter);
        $em->flush();
        $this->addFlash('success', 'Vous êtes maintenant inscrit à la newsletter!');
        return $this->redirectToRoute('newsletter');

    }






}
