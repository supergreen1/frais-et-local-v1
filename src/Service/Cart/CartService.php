<?php

namespace App\Service\Cart;

use App\Repository\ClientRepository;
use App\Repository\ProduitsRepository;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class CartService
{

    protected $session;
    protected $repository;
    protected $clientRepository;
    protected $request;
    protected $flashBag;
    protected $security;

    public function __construct(SessionInterface $session, ProduitsRepository $repository, RequestStack $request,
                                FlashBagInterface $flashBag, ClientRepository $clientRepository, Security $security)
    {
        $this->session = $session;
        $this->repository = $repository;
        $this->request = $request;
        $this->flashBag = $flashBag;
        $this->clientRepository = $clientRepository;
        $this->security = $security;
    }

    public function add(int $id)
    {


        $panier = $this->session->get('panier', []);
        if (!empty($panier[$id])) {

            if ($this->request->getCurrentRequest()->query->get('qte') != null) $panier[$id] = $this->request->getCurrentRequest()->query->get('qte');
            $panier[$id]++;

        } else {

            if ($this->request->getCurrentRequest()->query->get('qte') != null) {
                $panier[$id] = $this->request->getCurrentRequest()->query->get('qte');
            } else {
                $panier[$id] = 1;
            }
        }
        $this->session->set('panier', $panier);


    }


    public function remove(int $id)
    {

        $panier = $this->session->get('panier', []);

        if (!empty($panier[$id])) {
            unset($panier[$id]);
        }

        $this->session->set('panier', $panier);

    }

    public function edit(int $id)
    {

        $panier = $this->session->get('panier', []);
        if (!empty($panier[$id])) {
            if ($this->request->getCurrentRequest()->query->get('qte') != null) $panier[$id] = $this->request->getCurrentRequest()->query->get('qte');
            $panier[$id] = $this->request->getCurrentRequest()->query->get('qte');
            $this->flashBag->add('success', 'Quantité du produit modifié avec succès dans votre panier');
        } else {
            if ($this->request->getCurrentRequest()->query->get('qte') != null)
                $panier[$id] = $this->request->getCurrentRequest()->query->get('qte');
            else
                $panier[$id] = 1;
            $this->flashBag->add('success', 'Produit ajouté avec succès dans votre panier');
        }

        $this->session->set('panier', $panier);
    }


    public function getFullCart(): array
    {

        /*   Récupération des données et lui affecte un panier avec un tableau   */


        $panier = $this->session->get('panier', []);

        $panierWithData = [];

        /*   Boucle pour afficher les produit   */

        foreach ($panier as $id => $quantity) {
            $panierWithData[] = [
                'produit' => $this->repository->find($id),
                'quantity' => $quantity
            ];
        }

        return $panierWithData;
    }

    public function getSousTotal(): float
    {
        /*   Calcul Soustotal       */

        $sousTotal = 0;
        $totalTva = 0;
        foreach ($this->getFullCart() as $item) {
            $totalItem = $item['produit']->getPrice() * $item['quantity'];

            $tva = $totalItem * ($item['produit']->gettva() / 100);
            $simpleTotalTva = $totalItem + $tva;
            $totalTva += $simpleTotalTva;
            $sousTotal += $totalItem;


        }
        $this->session->set('sousTotal', $totalTva);
        return $sousTotal;

    }

    public function getTva(): float
    {

        /*   Calcul TVA        */

        $TVA = 0;
        if ($this->getFullCart() != null) {
            foreach ($this->getFullCart() as $item) {
                $totalItem = $item['produit']->getPrice() * $item['quantity'];
                $TVA += $totalItem * ($item['produit']->gettva() / 100);
            }
        }

        return $TVA;
    }

    public function getTotal(): float
    {
        /*   Calcul total       */

        if ($this->getSousTotal() == 0) {
            $total = 0;
        } else {

            $total = number_format($this->getSousTotal() + $this->getTva(), 2);

        }
        return $total;
    }

    public function payment(){
        Stripe::setApiKey('sk_test_AbaEYictyn5bOMQqLGvaV8cx00GnRvjt0W');

        $intent = PaymentIntent::create([
            'amount' => $this->getTotal() * 100,
            'currency' => 'eur',
            'receipt_email' => $this->clientRepository->findOneBy(['username' => $this->security->getUser()->getUsername()])->getEmail(),
            'metadata' => ['integration_check' => 'accept_a_payment'],
        ]);

        return $intent;
    }

}
