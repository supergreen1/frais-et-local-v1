<?php

namespace App\Controller;


use App\Entity\Client;
use App\Form\ResetPassType;
use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityClientController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @route("/oubli-client", name="forgotten_client")
     * @param Request $request
     * @param ClientRepository $repository
     * @param \Swift_Mailer $mailer
     * @param TokenGeneratorInterface $tokenGenerator
     * @return RedirectResponse|Response
     */

    public function forgettenClient(Request $request, ClientRepository $repository, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator)
    {

        //On créer le formulaire
        $form = $this->createForm(ResetPassType::class);

        // on traite le formulaire
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //on récupère les données
            $donnees = $form->getData();

            //on cheerche si cet utilisateur a cet email
            $clientrepo = $repository->findAll();
            $client = new Client();
            $number = count($clientrepo);

            for ($i = 0; $i <= $number - 1; $i++) {

                if ($donnees['email'] == $clientrepo[$i]->getEmail()) {
                    $client = $clientrepo[$i];
                    $token = $tokenGenerator->generateToken();

                    try {
                        $client->setResetToken($token);
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($client);
                        $entityManager->flush();
                    } catch (\Exception $e) {
                        $this->addFlash('danger', 'Une erreur est survenue : ' . $e->getMessage());
                        return $this->redirectToRoute('app_login');
                    }
                }
            }
//            dd($donnees['email'], $number , $client->getEmail()  );
            if ($client->getEmail() == null) {

                // On envoie un message flash
                $this->addFlash('danger', 'Cette adresse n\'existe pas');

                return $this->redirectToRoute('app_login');
            }


            //on génère l'URL de réinitialisation de mot de passe
            $url = $this->generateUrl('app_reset_password_client', ['token' => $token],
                UrlGeneratorInterface::ABSOLUTE_URL);
            // On envoie le message
            $message = (new \Swift_Message('Mot de passe oublié'))
                ->setFrom('votre@adresse.fr')
                ->setTo($client->getEmail())
                ->setBody(
                    "<p>Bonjour,</p><p>Pour activer votre mot de passe, veuillez cliquer sur le lien suivant : " . $url . '</p>',
                    'text/html'
                );
            $mailer->send($message);
            // On crée le message flash
            $this->addFlash('success', 'Un e-mail de réinitialisation a été envoyé sur votre boite mail');
            return $this->redirectToRoute('app_login');

        }
        // On envoie vers la page de demande de l'e-mail
        return $this->render('security/forgotten_password.html.twig', ['emailForm' => $form->createView()]);
    }

    /**
     * @Route("/reset-pass-client/{token}", name="app_reset_password_client")
     * @param $token
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return RedirectResponse|Response
     */

    public function resetPassword($token, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        //on va chercher l'utilisateur avec le token fourni

        $client = $this->getDoctrine()->getRepository(Client::class)->findOneBy(['reset_token' => $token]);

        if (!$client) {
            $this->addFlash('danger', 'Token inconnue');
            return $this->redirectToRoute('prod_login');
        }
        if ($request->isMethod('POST')) {
            $client->setPassword($passwordEncoder->encodePassword($client, $request->request->get('password')));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($client);
            $entityManager->flush();
            $this->addFlash('success', 'Mot de passe modifié avec succès');

            return $this->redirectToRoute('app_login');
        } else {
            return $this->render('security/reset_password.html.twig', ['token' => $token]);
        }

    }









}
