<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProduitsRepository")
 * @Vich\Uploadable
 */
class Produits
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column( length=128, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     */
    private $stock;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="boolean", options={"default" : true})
     */
    private $available = true;

    /**
     * @var DateTime $created_at
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @var DateTime $updated_at
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated_at;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $featured_image;

    /**
     * @Vich\UploadableField(mapping="featured_images", fileNameProperty="featured_image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tags", inversedBy="produits")
     */
    private $tags;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categories", inversedBy="produits")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categories;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Unite", inversedBy="produits")
     * @ORM\JoinColumn(nullable=false)
     */
    private $unite;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ListeCourse", mappedBy="Produit")
     */
    private $listeCourses;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producteur", inversedBy="produits")
     * @ORM\JoinColumn(nullable=false,
     *  onDelete="CASCADE"
     * )
     *
     */
    private $seller;

    /**
     * <<<<<<< Updated upstream
     * @ORM\ManyToMany(targetEntity=Commande::class, mappedBy="produit")
     */
    private $commandes;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notation", mappedBy="produit")
     */
    private $notations;


    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->listeCourses = new ArrayCollection();


        $this->commandes = new ArrayCollection();

        $this->notations = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->title;

    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAvailable(): ?bool
    {
        return $this->available;
    }

    public function setAvailable(bool $available): self
    {
        $this->available = $available;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }


    public function getFeaturedImage()
    {
        return $this->featured_image;
    }


    public function setFeaturedImage($featured_image)
    {
        $this->featured_image = $featured_image;

        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param File|null $image
     * @return void
     * @throws \Exception
     */
    public function setImageFile(File $image = null)
    {

        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updated_at = new \DateTime('now');
        }
    }

    /**
     * @return Collection|Tags[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tags $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tags $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    /**
     * @return Categories|null
     */

    public function getCategories(): ?Categories
    {
        return $this->categories;
    }

    public function setCategories(?Categories $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function getUnite(): ?Unite
    {
        return $this->unite;
    }

    public function setUnite(?Unite $unite): self
    {
        $this->unite = $unite;

        return $this;
    }

    /**
     * @return Collection|ListeCourse[]
     */
    public function getListeCourses(): Collection
    {
        return $this->listeCourses;
    }

    public function addListeCourse(ListeCourse $listeCourse): self
    {
        if (!$this->listeCourses->contains($listeCourse)) {
            $this->listeCourses[] = $listeCourse;
            $listeCourse->setProduit($this);
        }

        return $this;
    }

    public function removeListeCourse(ListeCourse $listeCourse): self
    {
        if ($this->listeCourses->contains($listeCourse)) {
            $this->listeCourses->removeElement($listeCourse);
            // set the owning side to null (unless already changed)
            if ($listeCourse->getProduit() === $this) {
                $listeCourse->setProduit(null);
            }
        }

        return $this;
    }

    public function getSeller(): ?Producteur
    {
        return $this->seller;
    }

    public function setSeller(?Producteur $seller): self
    {
        $this->seller = $seller;

        return $this;
    }

    /**
     * <<<<<<< Updated upstream
     * @return Collection|Commande[]
     */
    public function getCommandes(): Collection
    {
        return $this->commandes;
    }

    public function addCommande(Commande $commande): self
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->addProduit($this);
        }
    }


    /**
     * @return Collection|Notation[]
     */
    public function getNotations(): Collection
    {
        return $this->notations;
    }

    public function addNotation(Notation $notation): self
    {
        if (!$this->notations->contains($notation)) {
            $this->notations[] = $notation;
            $notation->setProduit($this);

        }

        return $this;
    }


    public function removeCommande(Commande $commande): self
    {
        if ($this->commandes->contains($commande)) {
            $this->commandes->removeElement($commande);
            $commande->removeProduit($this);
        }
    }
            public
            function removeNotation(Notation $notation): self
            {
                if ($this->notations->contains($notation)) {
                    $this->notations->removeElement($notation);
                    // set the owning side to null (unless already changed)
                    if ($notation->getProduit() === $this) {
                        $notation->setProduit(null);
                    }
                }
                return $this;
            }

        }
