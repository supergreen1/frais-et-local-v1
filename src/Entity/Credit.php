<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CreditRepository")
 */
class Credit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Producteur", inversedBy="credit", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $producteur;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateLastSell;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getProducteur(): ?Producteur
    {
        return $this->producteur;
    }

    public function setProducteur(Producteur $producteur): self
    {
        $this->producteur = $producteur;

        return $this;
    }

    public function getDateLastSell(): ?\DateTimeInterface
    {
        return $this->dateLastSell;
    }

    public function setDateLastSell(\DateTimeInterface $dateLastSell): self
    {
        $this->dateLastSell = $dateLastSell;

        return $this;
    }
}
