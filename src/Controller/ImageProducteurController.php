<?php

namespace App\Controller;

use App\Entity\ImageProducteur;

use App\Form\ImageProducteurType;
use App\Repository\ImageProducteurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
class ImageProducteurController extends AbstractController
{

    /**
     * @var ImageProducteurRepository
     */

    private $repository;

    /**
     * ShopController constructor.
     * @param ImageProducteurRepository $repository
     */

    public function __construct(ImageProducteurRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/image/producteur", name="image_producteur")
     * @Route("image/producteur/#image", name="producteur_image")
     * @param Request $request
     * @param UserInterface $user
     * @return Response
     */
    public function index(Request $request, UserInterface $user)
    {

        $imagerepo = $this->repository->findAll();
        $em = $this->getDoctrine()->getManager();
        $image = new ImageProducteur();
        $imageForm = $this->createForm(ImageProducteurType::class, $image);
        $imageForm->handleRequest($request);


        $number = count($imagerepo);

        // On cherche si le producteur a déjà une image sinon on mets la photo à null
            for ($i = 0; $i <= $number - 1; $i++) {

            if ($user->getId() == $imagerepo[$i]->getProducteur()) {
                $imagepro = $imagerepo[$i];
            }
            else{
                $imagepro = null;
            }
        }

        // On verifie qu'il y a au moins une photo en BDD tout producteurs confondus
        if (empty($imagerepo)) {
            $imagepro = null;
        }
        // On verifie si le producteur n'a pas déjà une image de profil
        if ($imageForm->isSubmitted() && $imageForm->isValid()) {
            for ($i = 0; $i <= $number - 1; $i++) {
                if ($imageForm->getData()->getProducteur() == $imagerepo[$i]->getProducteur()) {
                    $this->addFlash('danger', 'Vous devez supprimer votre image pour en ajouter une!');
                    return $this->redirectToRoute('producteur_image');
                }
            }
            $em->persist($image);
            $em->flush();
            return $this->redirectToRoute('producteur_image');
        }


        return $this->render('image_producteur/index.html.twig', [
            'controller_name' => 'ImageProducteurController',
            'imageForm' => $imageForm->createView(),
            'image' => $imagepro,


        ]);
    }


    /**
     * @Route("/{id}", name="imageProducteur_delete", methods={"DELETE"})
     * @param Request $request
     * @param ImageProducteur $imageProducteur
     * @return RedirectResponse
     */

    public function delete(Request $request, ImageProducteur $imageProducteur) : Response
    {
        if($this->isCsrfTokenValid('delete' .$imageProducteur->getId(), $request->request->get('_token'))){
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($imageProducteur);

            $entityManager->flush();

        }
       return $this->redirectToRoute('image_producteur');
    }
}
