<?php

namespace App\Controller;

use App\Entity\Commande;
use App\Entity\Credit;
use App\Entity\Ligne;

use App\Repository\LigneRepository;
use App\Service\Cart\CartService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CommandeController extends AbstractController
{
    /**
     * @Route("/commande", name="commande")
     */
    public function index()
    {
        return $this->render('commande/index.html.twig', [
            'controller_name' => 'CommandeController',
        ]);
    }

    /**
     * @param CartService $cartService
     * @param SessionInterface $session
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Exception
     * @Route("/commande/create", name="commande_create")
     */
    public function create(CartService $cartService, SessionInterface $session, EntityManagerInterface $em){

        $items = $cartService->getFullCart();
        $sousTotal = $cartService->getSousTotal();
        $total = $cartService->getTotal();
        $tva = $cartService->getTva();

        $commande = new Commande();

        foreach ($items as $item){
            $produit = $item['produit'];
            $quantite = $item['quantity'];

            $ligne = new Ligne();
            $ligne->setProduit($produit);
            $ligne->setQuantite($quantite);

            $ligne->getProduit()->setStock($ligne->getProduit()->getStock()-$quantite);

            if($ligne->getProduit()->getSeller()->getCredit()== null){

                $credit = new Credit();
                $credit->setProducteur($ligne->getProduit()->getSeller());
                $credit->setAmount(($ligne->getProduit()->getPrice()* $ligne->getQuantite()));
                $credit->setDateLastSell(new \DateTime());

            }
            else{
                $credit = $ligne->getProduit()->getSeller()->getCredit();
                $credit->setAmount($credit->getAmount()+($ligne->getProduit()->getPrice()*$ligne->getQuantite()));
                $credit->setDateLastSell(new \DateTime());
            }

            $commande->setClient($this->getUser());
            $commande->setDateCommande(new \DateTime());
            $commande->addProduit($produit);
            $commande->addLigne($ligne);

            $em->persist($credit);

        }
        $commande->setTotal($total);
        $commande->setTva($tva);
        $commande->setSecretClient($cartService->payment()->client_secret);

        $em->persist($commande);
        $em->flush();

        $session->remove('panier');



        return $this->render('commande/create.html.twig',[

        ]);
    }


    /**
     * @param EntityManagerInterface $em
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/commande/detail/{id}", name="commande_details", requirements={"id"="\d+"}, methods={"GET","POST"})
     */
    public function details(EntityManagerInterface $em, $id){

        $commande= $em->getRepository(Commande::class)->find($id);

        if($commande == null){
            $this->createNotFoundException('Commande inexistante');
        }


        return $this->render('commande/details.html.twig',[
            'commande'=>$commande,

        ]);
    }

    /**
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/commande/mescommandes", name="commandes_mescommandes")
     */
    public function mesCommandes(EntityManagerInterface $em){

        // permet de voir la liste de commande faite par le client connecté

        $commandes = $em->getRepository(Commande::class)->findMycommandes($this->getUser());

        return $this->render('commande/mesCommandes.html.twig',[
            'commandes'=>$commandes,
        ]);
    }

}
