<?php

namespace App\Controller;
use App\Entity\Client;
use App\Entity\ListeCourse;
use App\Entity\Produits;
use App\Repository\ListeCourseRepository;
use App\Repository\ProduitsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
class WishlistController extends AbstractController
{


    /**
     * @var ProduitsRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * ShopController constructor.
     * @param ListeCourseRepository $repository
     * @param EntityManagerInterface $em
     * @param SessionInterface $session
     */

    public function __construct(ListeCourseRepository $repository, EntityManagerInterface $em, SessionInterface $session)
    {
        $this->repository = $repository;
        $this->em = $em;
        $this->session = $session;

    }

    /**
     * @Route("/wishlist", name="wishlist")
     * @param UserInterface $user
     * @return Response
     */
    public function index(UserInterface $user)

    {

            $liste = $this->repository->findListe();
            $listeClient = [];
            $i = 0;

        foreach ($liste as $id => $quantity) {
                $listeboucle = $liste[$i]->getClient()->getId();
            if($listeboucle == $user->getId()){
                $listeClient [] =  $liste[$i]->getproduit();
            }
            $i++;
        }


return $this->render('wishlist/index.html.twig', [
'produits' => $listeClient,


]);

    }

    /**
     * @route("/wishlist/add{id}", name="wishlist_add")
     * @param $id
     * @param EntityManagerInterface $em
     * @param UserInterface $user
     * @return Response
     */

    //  Envoie du produit dans la liste de course

    public function add($id, EntityManagerInterface $em, UserInterface $user, ProduitsRepository $repository)
    {


        /*   Récupération dans la BDD   */

        $produit = $this->getDoctrine()->getRepository(Produits::class)->find($id);
        $client = $this->getDoctrine()->getRepository(Client::class)->find($user);


        $listes = $this->repository->findListe();

        /*   Récupération de l'id de l'utilisateur connecté   */

        /*   Préparation insertion en BDD   */
        $liste = new ListeCourse();

        $liste->setQuantity(1);
        $liste->setProduit($produit);
        $liste->setClient($client);
        $idAjout = $liste->getProduit()->getId();

        /*   Vérification si le produit est dans la liste   */
        $compare = [];
        $i = 0;
        foreach ($listes as $id => $quantity) {
            $compare[] = [
                $idProduit = $listes[$i]->getProduit()->getId(),
                $idClient = $listes[$i]->getClient()->getId()
            ];
            $i++;
            if ($idProduit == $idAjout && $idClient == $user->getId() ) {

                $this->addFlash('danger', 'Produit déjà dans votre liste de course!');
                return $this->redirectToRoute('shop');
            }
        }

        /*   Insertion en BDD   */
        $em->persist($liste);
        $em->flush();
        $this->addFlash('success', 'Produit ajouté à votre liste de course!');
        return $this->redirectToRoute('shop');
    }



    /**
     * @Route("/wishlist/panier{id}", name="panier")
     * @param $id
     * @param SessionInterface $session
     * @param Request $request
     * @return Response
     */

    //  Envoie du produit de la liste de course au panier avec sa quantité


    public function add_cart($id, SessionInterface $session, Request $request, ProduitsRepository $repository)
    {

        $panier = $session->get('panier', []);
        if (!empty($panier[$id])) {
            if ($request->query->get('qte') != null) $panier[$id] = $request->query->get('qte');
            $panier[$id] = $request->query->get('qte');
            $this->get('session')->getFlashBag()->add('success', 'Quantité du produit modifié avec succès dans votre panier');
        } else {
            if ($request->query->get('qte') != null)
                $panier[$id] = $request->query->get('qte');
            else
                $panier[$id] = 1;
            $this->get('session')->getFlashBag()->add('success', 'Produit ajouté avec succès dans votre panier');

        }
        $session->set('panier', $panier);
        $panierWithData = [];

        /*   Boucle pour afficher les produit   */

        foreach ($panier as $id => $quantity) {
            $panierWithData[] = [
                'produit' => $repository->find($id),
                'quantity' => $quantity
            ];
        }
        /*   Calcul Soustotal       */

        $sousTotal = 0;
        foreach ($panierWithData as $item) {
            $totalItem = $item['produit']->getPrice() * $item['quantity'];
            $sousTotal += $totalItem;
        }
        $session->set('sousTotal', $sousTotal);
        return $this->redirectToRoute("wishlist");

    }

    /**
     * @route("/wishlist/remove/{id}", name="wishlist_remove")
     * @param $id
     * @param UserInterface $user
     * @return RedirectResponse
     */
    public function remove($id, UserInterface $user)
    {

        $produit = $this->getDoctrine()->getRepository(Produits::class)->find($id);
        $listes = $this->repository->findListe();
        $idRemove = $produit->getId();
        $em = $this->getDoctrine()->getManager();
        $compare = [];
        $i = 0;
        foreach ($listes as $id => $quantity) {
            $compare[] = [
                $idProduit = $listes[$i],
                $idClient = $listes[$i]->getClient(),
            ];
            $i++;
            //            dd($idProduit->getProduit()->getId(), $idRemove,$idClient, $user);
            if ($idProduit->getProduit()->getId() ==  $idRemove && $idClient == $user) {
                $em->remove($idProduit);
                $em->flush();
                return $this->redirectToRoute('wishlist');
            }
        }
    }
}
