<?php

namespace App\Form;

use App\Entity\Produits;
use Doctrine\ORM\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProduitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,[
                'label' => 'Nom du produit',

            ])
            ->add('description', CKEditorType::class)
            ->add('stock', TextType::class,[
        'label' => 'Quantité en stock',

    ])
            ->add('price', TextType::class,[
                'label' => 'Prix',

            ])
            ->add('imageFile', FileType::class, [
                'required' => false,
                'label' => 'Image du produit',
            ])
            ->add('categories', EntityType::class, [
                'class' => "App\Entity\Categories",
                'choice_label' => "name",
                'placeholder' => "Sélectionner une catégorie",
                'expanded' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'desc');
                }
            ])

            ->add('tags', EntityType::class, [
                'class' => "App\Entity\Tags",
                'choice_label' => 'name',
                'label' => 'Tags',
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('unite', EntityType::class, [
                'class' => "App\Entity\Unite",
                'choice_label' => "name",
                'placeholder' => "Sélectionner l'unité",
                'expanded' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'desc');
                }
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Produits::class,
        ]);
    }
}
