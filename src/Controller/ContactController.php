<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ProduitsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{

    /**
     * @Route("/contact", name="contact")
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @param SessionInterface $session
     * @param ProduitsRepository $repository
     * @return RedirectResponse|Response
     */
    public function index(Request $request, \Swift_Mailer $mailer, SessionInterface $session, ProduitsRepository $repository)
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        /**Pour activer l'interception du mail en environement dev
         *Aller dans config/package/dev/web_profiler.yaml
         * puis mettre intercept_redirects: true
         */

        if ($form->isSubmitted() && $form->isValid()) {
            $contact = $form->getData();

            //ici on envoie le mail
            dump($contact);
            $message = (new\Swift_Message('Nouveau Contact'))

                //On attribue l'expéditeur
                ->setFrom($contact->getEmail())

                //On attribue le destinataire
                ->setTo('mickael.coconnier@gmial.com')

                //On crée le message avec la vue Twig
                ->setBody(
                    $this->renderView(
                        'emails/contact.html.twig', compact('contact')
                    ),
                    'text/html'
                );

            //On envoie le message
            $mailer->send($message);
            $this->addFlash('message', 'Le message a bien été envoyé');
            return $this->redirectToRoute('contact');
        }
        return $this->render('contact/index.html.twig', [
            'controller_name' => 'ContactController',
            'form' => $form->createView()

        ]);
    }

    /**
     * @route("/contact/map", name="contact_map")
     * @param Request $request
     * @param SessionInterface $session
     * @param ProduitsRepository $repository
     * @return Response
     */

    public function map(Request $request, SessionInterface $session, ProduitsRepository $repository)
    {

        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        return $this->render('contact/index.html.twig', [
            'form' => $form->createView()
        ]);

    }


}



