<?php

namespace App\Repository;

use App\Entity\ImageProducteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ImageProducteur|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImageProducteur|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImageProducteur[]    findAll()
 * @method ImageProducteur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageProducteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ImageProducteur::class);
    }

    // /**
    //  * @return ImageProducteur[] Returns an array of ImageProducteur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ImageProducteur
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
