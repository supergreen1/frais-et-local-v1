<?php

namespace App\Controller;

use App\Entity\Produits;
use App\Form\ProduitType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProduitController extends AbstractController
{
    /**
     * @Route("/produit", name="produit")
     */
    public function index()
    {
        return $this->render('produit/index.html.twig', [
            'controller_name' => 'ProduitController',
        ]);
    }

    /**
     * @param EntityManagerInterface $em
     * @param Request $request
     * @return RedirectResponse|Response
     * @Route("/produit/add", name="produit_add")
     */
    public function add(EntityManagerInterface $em, Request $request)
    {

        $produit = new Produits();
        $produitForm = $this->createForm(ProduitType::class, $produit);
        $produitForm->handleRequest($request);

        //dd($produit);

        if ($produitForm->isSubmitted() && $produitForm->isValid()) {
            if ($this->getUser() != null) {

                $produit->setAvailable(true);
                $produit->setSeller($this->getUser());


                $em->persist($produit);
                $em->flush();

                return $this->redirectToRoute('user_producteur');
            }
        }
        return $this->render('produit/add.html.twig', [
            'produitForm' => $produitForm->createView()
        ]);
    }

    /**
     * @param $id
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/produit/{id}", name="produit_detail", requirements={"id"="\d+"}, methods={"GET","POST"})
     */
    public function detail($id, EntityManagerInterface $em)
    {

        $produit = $em->getRepository(Produits::class)->find($id);

        if ($produit == null) {
            throw $this->createNotFoundException('produit inconnu');
        }

        return $this->render('produit/detail.html.twig', [
            'produit' => $produit
        ]);
    }

    /**
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Response
     * @Route("/produit/{id}/edit", name="produit_edit", requirements={"id"="\d+"}, methods={"GET","POST"})
     */
    public function edit(EntityManagerInterface $em, Request $request, $id)
    {

        $produit = $em->getRepository(Produits::class)->find($id);

        $produitForm = $this->createForm(ProduitType::class, $produit);

        $produitForm->handleRequest($request);


        if ($produitForm->isSubmitted() && $produitForm->isValid()) {
            if ($this->getUser() != null) {
                if ($produit->getStock() == 0) {
                    $produit->setAvailable(false);
                }


                $em->persist($produit);
                $em->flush();

                return $this->redirectToRoute('shop_detail', ['id' => $produit->getId()]);
            }
        }

        return $this->render('produit/edit.html.twig', [

            'produitForm' => $produitForm->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="produit_delete", methods={"DELETE"})
     * @param Request $request
     * @param Produits $produit
     * @return Response
     */
    public function delete(Request $request, Produits $produit): Response
    {
        if ($this->isCsrfTokenValid('delete' . $produit->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($produit);
            $entityManager->flush();
        }
        return $this->redirectToRoute('user_producteur');
    }
}

