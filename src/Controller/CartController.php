<?php

namespace App\Controller;

use App\Repository\ClientRepository;
use App\Service\Cart\CartService;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{

    /**
     * @Route("/cart", name="cart")
     * @return Response
     */
    public function index(CartService $cartService)
    {
       $cartService->getFullCart();
       $cartService->getSousTotal();
       $cartService->getTva();
        $cartService->getTotal();

        return $this->render('cart/index.html.twig', [
            'controller_name' => 'CartController',
            'produits' => $cartService->getFullCart(),
            'sousTotal' => $cartService->getSousTotal(),
            'TVA' => $cartService->getTva(),
            'total' => $cartService->getTotal()
        ]);
    }

    /**
     *
     * @Route("/cart/edit{id}", name="cart_edit")
     * @param $id
     * @param CartService $cartService
     * @return RedirectResponse
     */

    public function edit($id, CartService $cartService)
    {
        $cartService->edit($id);

        return $this->redirectToRoute('cart');
    }


    /**
     * @Route("/cart/add{id}", name="cart_add")
     * @param $id
     * @param CartService $cartService
     * @return RedirectResponse
     */

    public function add($id, CartService $cartService)
    {
       $cartService->add($id);

        return $this->redirectToRoute('shop');
    }

    /**
     * @route("/cart/remove/{id}", name="cart_remove")
     * @param $id
     * @param CartService $cartService
     * @return RedirectResponse
     */
    public function remove($id, CartService $cartService)
    {

        $cartService->remove($id);
        return $this->redirectToRoute("cart");
    }

    /**
     * @param CartService $cartService
     * @return Response
     * @throws ApiErrorException
     * @Route("/cart/payment", name="cart_payment")
     */
    public function payment(CartService $cartService){

        $cartService->payment();

        return $this->render('cart/payment.html.twig',[
            'clientSecret'=>$cartService->payment()->client_secret
        ]);
    }

}
